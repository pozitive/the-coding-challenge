# Robot's state
State = Struct.new(:x, :y, :f) do
  DIRECTION = %w(north east south west).freeze

  def initialize(x, y, f)
    f.downcase!
    super
  end

  def valid?
    if valid_point(x, y) && valid_facing(f)
      true
    else
      false
    end
  end

  private

  def valid_point(x, y)
    true if x.between?(0, 4) && y.between?(0, 4)
  end

  def valid_facing(f)
    true if DIRECTION.include? f
  end
end
