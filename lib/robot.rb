require 'state'

# Toy Robot Simulator
class Robot
  attr_accessor :state

  def initialize
    @state = 'unplaced'
  end

  def place(x, y, f)
    st = State.new(x, y, f)
    @state = st if st.valid?
  end

  def step
    case state.f
    when 'north'
      State.new(state.x, state.y + 1, state.f)
    when 'east'
      State.new(state.x + 1, state.y, state.f)
    when 'south'
      State.new(state.x, state.y - 1, state.f)
    when 'west'
      State.new(state.x - 1, state.y, state.f)
    end
  end

  def move
    @state = step if step.valid?
  end

  def left
    i = DIRECTION.index(state.f) - 1
    i = 3 if i < 0
    @state.f = DIRECTION[i]
  end

  def right
    i = DIRECTION.index(state.f) + 1
    i = 0 if i > 3
    @state.f = DIRECTION[i]
  end

  def report
    "The Robot in postion X = #{state.x}, Y = #{state.y} "\
    "and facing #{state.f.upcase}"
  end
end
