require 'pry'
require 'robot'

describe Robot do
  let(:robot) { described_class.new }

  describe 'initialize' do
    it { expect(robot).to be_an_instance_of Robot }
    it { expect(robot.state).to be_an_instance_of String }
    it { expect(robot.state).to eq 'unplaced' }
  end

  describe '#place' do
    before 'example' do
      robot.place(0, 0, 'NORTH')
    end
    it { expect(robot.state).to be_an_instance_of State }
    it { expect(robot.state).to eq State.new(0, 0, 'north') }
  end

  describe '#step' do
    context 'step to north' do
      before 'example' do
        robot.place(1, 1, 'north')
      end
      it { expect(robot.step).to eq State.new(1, 2, 'north') }
    end

    context 'step to east' do
      before 'example' do
        robot.place(1, 1, 'east')
        robot.step
      end
      it { expect(robot.step).to eq State.new(2, 1, 'east') }
    end

    context 'step to south' do
      before 'example' do
        robot.place(1, 1, 'south')
        robot.step
      end
      it { expect(robot.step).to eq State.new(1, 0, 'south') }
    end

    context 'step to west' do
      before 'example' do
        robot.place(1, 1, 'west')
        robot.step
      end
      it { expect(robot.step).to eq State.new(0, 1, 'west') }
    end
  end

  describe '#move' do
    context 'to the north succesfully' do
      before 'example' do
        robot.place(0, 0, 'north')
        robot.move
      end
      it { expect(robot.state.x).to eq 0 }
      it { expect(robot.state.y).to eq 1 }
      it { expect(robot.state.f).to eq 'north' }
    end

    context 'to the north unsuccesfully' do
      before 'example' do
        robot.place(0, 4, 'north')
        robot.move
      end
      it { expect(robot.state.x).to eq 0 }
      it { expect(robot.state.y).to eq 4 }
      it { expect(robot.state.f).to eq 'north' }
    end

    context 'to the east succesfully' do
      before 'example' do
        robot.place(0, 0, 'east')
        robot.move
      end
      it { expect(robot.state.x).to eq 1 }
      it { expect(robot.state.y).to eq 0 }
      it { expect(robot.state.f).to eq 'east' }
    end

    context 'to the north unsuccesfully' do
      before 'example' do
        robot.place(0, 0, 'west')
        robot.move
      end
      it { expect(robot.state.x).to eq 0 }
      it { expect(robot.state.y).to eq 0 }
      it { expect(robot.state.f).to eq 'west' }
    end
  end

  describe '#left' do
    context('place north') do
      before 'example' do
        robot.place(0, 0, 'NORTH')
        robot.left
      end
      it { expect(robot.state).to eq State.new(0, 0, 'west') }
    end

    context('place west') do
      before 'example' do
        robot.place(0, 0, 'west')
        robot.left
      end
      it { expect(robot.state).to eq State.new(0, 0, 'south') }
    end

    context('place south') do
      before 'example' do
        robot.place(0, 0, 'south')
        robot.left
      end
      it { expect(robot.state).to eq State.new(0, 0, 'east') }
    end

    context('place east') do
      before 'example' do
        robot.place(0, 0, 'east')
        robot.left
      end
      it { expect(robot.state).to eq State.new(0, 0, 'north') }
    end
  end

  describe '#right' do
    context('place north') do
      before 'example' do
        robot.place(0, 0, 'NORTH')
        robot.right
      end
      it { expect(robot.state).to eq State.new(0, 0, 'east') }
    end

    context('place west') do
      before 'example' do
        robot.place(0, 0, 'west')
        robot.right
      end
      it { expect(robot.state).to eq State.new(0, 0, 'north') }
    end

    context('place south') do
      before 'example' do
        robot.place(0, 0, 'south')
        robot.right
      end
      it { expect(robot.state).to eq State.new(0, 0, 'west') }
    end

    context('place east') do
      before 'example' do
        robot.place(0, 0, 'east')
        robot.right
      end
      it { expect(robot.state).to eq State.new(0, 0, 'south') }
    end
  end

  describe '#report' do
    context('report') do
      let(:str) { 'The Robot in postion X = 0, Y = 0 and facing NORTH' }
      before 'example' do
        robot.place(0, 0, 'NORTH')
      end
      it { expect(robot.report).to eq str }
    end
  end

  describe 'case' do
    it 'A' do
      robot.place(0, 0, 'NORTH')
      robot.move
      str = 'The Robot in postion X = 0, Y = 1 and facing NORTH'
      expect(robot.report).to eq str
    end

    it 'B' do
      robot.place(0, 0, 'NORTH')
      robot.left
      str = 'The Robot in postion X = 0, Y = 0 and facing WEST'
      expect(robot.report).to eq str
    end

    it 'C' do
      robot.place(1, 2, 'EAST')
      robot.move
      robot.move
      robot.left
      robot.move
      str = 'The Robot in postion X = 3, Y = 3 and facing NORTH'
      expect(robot.report).to eq str
    end
  end
end
